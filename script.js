let movie = false;
check()


function check() {
  if (movie == false) {
    document.getElementById("vid").style.display = "none";
    document.getElementById("text").style.display = "block";
    document.getElementById("header").style.display = "block";
    document.getElementById("button").style.display = "block";

  }
  else {
    document.getElementById("vid").style.display = "block";
    document.getElementById("text").style.display = "none";
    document.getElementById("header").style.display = "none";
    document.getElementById("button").style.display = "none";
  }
}

function playVideo(videoUrl) {
  document.getElementById("collapse").style.height = "20vh";
  movie = true
  check();
  document.getElementById("vid").src = videoUrl;
}

var coll = document.getElementsByClassName("collapsible");
for (var i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}